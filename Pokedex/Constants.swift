//
//  Constants.swift
//  Pokedex
//
//  Created by Bharath Narendra on 2/21/16.
//  Copyright © 2016 Bharath Narendra. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

public typealias DownloadComplete = () -> ()