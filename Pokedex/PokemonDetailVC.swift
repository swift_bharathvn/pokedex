//
//  PokemonDetailVC.swift
//  Pokedex
//
//  Created by Bharath Narendra on 2/20/16.
//  Copyright © 2016 Bharath Narendra. All rights reserved.
//

import UIKit

class PokemonDetailVC: UIViewController {

    var pokemon: Pokemon!
    
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var mainImage: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var defenceLabel: UILabel!
    @IBOutlet var heightLabel: UILabel!
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var baseAttackLabel: UILabel!
    @IBOutlet var currentEvo: UIImageView!
    @IBOutlet var nextEvo: UIImageView!
    @IBOutlet var evoText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLbl.text = pokemon.name.capitalizedString
        let img = UIImage(named: "\(pokemon.pokedexId)")
        mainImage.image = img
        currentEvo.image = img
        
        
        pokemon.downloadPokemonDetails { () -> () in
            //this will be called after download is done.
            self.updateUI()
        }
    }
    
    func updateUI(){
        descriptionLabel.text = pokemon.description
        typeLabel.text = pokemon.type
        defenceLabel.text = pokemon.defense
        heightLabel.text = pokemon.height
        idLabel.text = "\(pokemon.pokedexId)"
        weightLabel.text = pokemon.weight
        baseAttackLabel.text = pokemon.attack
        
        if pokemon.nextEvolutionId == "" {
            evoText.text = "No Evolutions"
            nextEvo.hidden = true
        } else {
            nextEvo.hidden = false
            nextEvo.image = UIImage(named: pokemon.nextEvolutionId)
            var str = "Next Evolution: \(pokemon.nextEvolutionText)"

            if pokemon.nextEvolutionLvl != "" {
                str += " - LVL \(pokemon.nextEvolutionLvl)"
            }
        }
        

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

}
